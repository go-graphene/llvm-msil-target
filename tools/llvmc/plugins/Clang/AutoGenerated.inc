//===- TableGen'erated file -------------------------------------*- C++ -*-===//
//
// LLVMC Configuration Library
//
// Automatically generated file, do not edit!
//
//===----------------------------------------------------------------------===//

#include "llvm/CompilerDriver/BuiltinOptions.h"
#include "llvm/CompilerDriver/CompilationGraph.h"
#include "llvm/CompilerDriver/ForceLinkageMacros.h"
#include "llvm/CompilerDriver/Plugin.h"
#include "llvm/CompilerDriver/Tool.h"

#include "llvm/Support/CommandLine.h"
#include "llvm/Support/raw_ostream.h"

#include <algorithm>
#include <cstdlib>
#include <iterator>
#include <stdexcept>

using namespace llvm;
using namespace llvmc;

extern cl::opt<std::string> OutputFilename;

inline const char* checkCString(const char* s)
{ return s == NULL ? "" : s; }

extern cl::opt<bool> AutoGeneratedSwitch_c;
extern cl::list<std::string> AutoGeneratedList_include;
extern cl::opt<bool> AutoGeneratedSwitch_E;
cl::opt<bool> AutoGeneratedSwitch_clang("clang"
, cl::desc("Use Clang instead of llvm-gcc"));

extern cl::list<std::string> AutoGeneratedList_Wa_comma_;
extern cl::opt<bool> AutoGeneratedSwitch_pthread;
extern cl::list<std::string> AutoGeneratedList_I;
extern cl::list<std::string> AutoGeneratedList_L;
extern cl::list<std::string> AutoGeneratedList_l;
extern cl::list<std::string> AutoGeneratedList_Wl_comma_;
extern cl::opt<bool> AutoGeneratedSwitch_S;
extern cl::opt<bool> AutoGeneratedSwitch_emit_dash_llvm;
extern cl::opt<bool> AutoGeneratedSwitch_fsyntax_dash_only;
extern cl::list<std::string> AutoGeneratedSinkOption;

namespace {

void PreprocessOptionsLocal() {
}

void PopulateLanguageMapLocal(LanguageMap& langMap) {
    langMap["cc"] = "c++";
    langMap["cp"] = "c++";
    langMap["cxx"] = "c++";
    langMap["cpp"] = "c++";
    langMap["CPP"] = "c++";
    langMap["c++"] = "c++";
    langMap["C"] = "c++";
    langMap["c"] = "c";
    langMap["m"] = "objective-c";
    langMap["i"] = "c-cpp-output";
    langMap["mi"] = "objective-c-cpp-output";
}

class as : public Tool {
private:
    static const char* InputLanguages_[];

public:
    const char* Name() const {
        return "as";
    }

    const char** InputLanguages() const {
        return InputLanguages_;
    }

    const char* OutputLanguage() const {
        return "object-code";
    }

    bool IsJoin() const {
        return false;
    }

    bool WorksOnEmpty() const {
        return false;
    }

    Action GenerateAction(const PathVector& inFiles,
        bool HasChildren,
        const llvm::sys::Path& TempDir,
        const InputLanguagesSet& InLangs,
        const LanguageMap& LangMap) const
    {
        throw std::runtime_error("as is not a Join tool!");
    }

    Action GenerateAction(const sys::Path& inFile,
        bool HasChildren,
        const llvm::sys::Path& TempDir,
        const InputLanguagesSet& InLangs,
        const LanguageMap& LangMap) const
    {
        std::string cmd;
        std::string out_file;
        std::vector<std::pair<unsigned, std::string> > vec;
        bool stop_compilation = !HasChildren;
        bool no_out_file = false;
        const char* output_suffix = "o";

        cmd = "as";

        if (!AutoGeneratedList_Wa_comma_.empty()) {
            for (cl::list<std::string>::iterator B = AutoGeneratedList_Wa_comma_.begin(), 
                 E = AutoGeneratedList_Wa_comma_.end(); B != E; ++B)
            {
                unsigned pos = AutoGeneratedList_Wa_comma_.getPosition(B - AutoGeneratedList_Wa_comma_.begin());
                vec.push_back(std::make_pair(pos, *B));
            }
        }
        if (AutoGeneratedSwitch_c) {
            stop_compilation = true;
        }

        vec.push_back(std::make_pair(InputFilenames.getPosition(0), inFile.str()));
        if (!no_out_file) {
            vec.push_back(std::make_pair(65536, "-o"));
            out_file = this->OutFilename(inFile,
                TempDir, stop_compilation, output_suffix).str();

            vec.push_back(std::make_pair(65536, out_file));
        }

        return Action(cmd, this->SortArgs(vec), stop_compilation, out_file);
    }

};
const char* as::InputLanguages_[] = {"assembler", 0};

class clang_c : public Tool {
private:
    static const char* InputLanguages_[];

public:
    const char* Name() const {
        return "clang_c";
    }

    const char** InputLanguages() const {
        return InputLanguages_;
    }

    const char* OutputLanguage() const {
        return "llvm-bitcode";
    }

    bool IsJoin() const {
        return false;
    }

    bool WorksOnEmpty() const {
        return false;
    }

    Action GenerateAction(const PathVector& inFiles,
        bool HasChildren,
        const llvm::sys::Path& TempDir,
        const InputLanguagesSet& InLangs,
        const LanguageMap& LangMap) const
    {
        throw std::runtime_error("clang_c is not a Join tool!");
    }

    Action GenerateAction(const sys::Path& inFile,
        bool HasChildren,
        const llvm::sys::Path& TempDir,
        const InputLanguagesSet& InLangs,
        const LanguageMap& LangMap) const
    {
        std::string cmd;
        std::string out_file;
        std::vector<std::pair<unsigned, std::string> > vec;
        bool stop_compilation = !HasChildren;
        bool no_out_file = false;
        const char* output_suffix = "bc";

        cmd = "clang";
        vec.push_back(std::make_pair(0, "-x"));
        vec.push_back(std::make_pair(0, "c"));

        if (AutoGeneratedSwitch_E) {
            vec.push_back(std::make_pair(AutoGeneratedSwitch_E.getPosition(), "-E"));
            stop_compilation = true;
            output_suffix = "i";
        }
        if ((AutoGeneratedSwitch_E)
             && (OutputFilename.empty())) {
            no_out_file = true;
        }
        if (AutoGeneratedSwitch_fsyntax_dash_only) {
            stop_compilation = true;
        }
        if (AutoGeneratedSwitch_S && AutoGeneratedSwitch_emit_dash_llvm) {
            vec.push_back(std::make_pair(65536, "-emit-llvm"));
            stop_compilation = true;
            output_suffix = "ll";
        }
        if (! (AutoGeneratedSwitch_S && AutoGeneratedSwitch_emit_dash_llvm)) {
            vec.push_back(std::make_pair(65536, "-emit-llvm-bc"));
        }
        if (AutoGeneratedSwitch_c && AutoGeneratedSwitch_emit_dash_llvm) {
            stop_compilation = true;
        }
        if (!AutoGeneratedList_include.empty()) {
            for (cl::list<std::string>::iterator B = AutoGeneratedList_include.begin(),
            E = AutoGeneratedList_include.end() ; B != E;) {
                unsigned pos = AutoGeneratedList_include.getPosition(B - AutoGeneratedList_include.begin());
                vec.push_back(std::make_pair(pos, "-include"));
                vec.push_back(std::make_pair(pos, *B));
                ++B;
            }
        }
        if (!AutoGeneratedList_I.empty()) {
            for (cl::list<std::string>::iterator B = AutoGeneratedList_I.begin(),
            E = AutoGeneratedList_I.end() ; B != E;) {
                unsigned pos = AutoGeneratedList_I.getPosition(B - AutoGeneratedList_I.begin());
                vec.push_back(std::make_pair(pos, "-I"));
                vec.push_back(std::make_pair(pos, *B));
                ++B;
            }
        }

        vec.push_back(std::make_pair(InputFilenames.getPosition(0), inFile.str()));
        if (!no_out_file) {
            vec.push_back(std::make_pair(65536, "-o"));
            out_file = this->OutFilename(inFile,
                TempDir, stop_compilation, output_suffix).str();

            vec.push_back(std::make_pair(65536, out_file));
        }

        if (!AutoGeneratedSinkOption.empty()) {
            for (cl::list<std::string>::iterator B = AutoGeneratedSinkOption.begin(), E = AutoGeneratedSinkOption.end(); B != E; ++B)
                vec.push_back(std::make_pair(AutoGeneratedSinkOption.getPosition(B - AutoGeneratedSinkOption.begin()), *B));
        }
        return Action(cmd, this->SortArgs(vec), stop_compilation, out_file);
    }

};
const char* clang_c::InputLanguages_[] = {"c", 0};

class clang_cpp : public Tool {
private:
    static const char* InputLanguages_[];

public:
    const char* Name() const {
        return "clang_cpp";
    }

    const char** InputLanguages() const {
        return InputLanguages_;
    }

    const char* OutputLanguage() const {
        return "llvm-bitcode";
    }

    bool IsJoin() const {
        return false;
    }

    bool WorksOnEmpty() const {
        return false;
    }

    Action GenerateAction(const PathVector& inFiles,
        bool HasChildren,
        const llvm::sys::Path& TempDir,
        const InputLanguagesSet& InLangs,
        const LanguageMap& LangMap) const
    {
        throw std::runtime_error("clang_cpp is not a Join tool!");
    }

    Action GenerateAction(const sys::Path& inFile,
        bool HasChildren,
        const llvm::sys::Path& TempDir,
        const InputLanguagesSet& InLangs,
        const LanguageMap& LangMap) const
    {
        std::string cmd;
        std::string out_file;
        std::vector<std::pair<unsigned, std::string> > vec;
        bool stop_compilation = !HasChildren;
        bool no_out_file = false;
        const char* output_suffix = "bc";

        cmd = "clang";
        vec.push_back(std::make_pair(0, "-x"));
        vec.push_back(std::make_pair(0, "c++"));

        if (AutoGeneratedSwitch_E) {
            vec.push_back(std::make_pair(AutoGeneratedSwitch_E.getPosition(), "-E"));
            stop_compilation = true;
            output_suffix = "i";
        }
        if ((AutoGeneratedSwitch_E)
             && (OutputFilename.empty())) {
            no_out_file = true;
        }
        if (AutoGeneratedSwitch_fsyntax_dash_only) {
            stop_compilation = true;
        }
        if (AutoGeneratedSwitch_S && AutoGeneratedSwitch_emit_dash_llvm) {
            vec.push_back(std::make_pair(65536, "-emit-llvm"));
            stop_compilation = true;
            output_suffix = "ll";
        }
        if (! (AutoGeneratedSwitch_S && AutoGeneratedSwitch_emit_dash_llvm)) {
            vec.push_back(std::make_pair(65536, "-emit-llvm-bc"));
        }
        if (AutoGeneratedSwitch_c && AutoGeneratedSwitch_emit_dash_llvm) {
            stop_compilation = true;
        }
        if (!AutoGeneratedList_include.empty()) {
            for (cl::list<std::string>::iterator B = AutoGeneratedList_include.begin(),
            E = AutoGeneratedList_include.end() ; B != E;) {
                unsigned pos = AutoGeneratedList_include.getPosition(B - AutoGeneratedList_include.begin());
                vec.push_back(std::make_pair(pos, "-include"));
                vec.push_back(std::make_pair(pos, *B));
                ++B;
            }
        }
        if (!AutoGeneratedList_I.empty()) {
            for (cl::list<std::string>::iterator B = AutoGeneratedList_I.begin(),
            E = AutoGeneratedList_I.end() ; B != E;) {
                unsigned pos = AutoGeneratedList_I.getPosition(B - AutoGeneratedList_I.begin());
                vec.push_back(std::make_pair(pos, "-I"));
                vec.push_back(std::make_pair(pos, *B));
                ++B;
            }
        }

        vec.push_back(std::make_pair(InputFilenames.getPosition(0), inFile.str()));
        if (!no_out_file) {
            vec.push_back(std::make_pair(65536, "-o"));
            out_file = this->OutFilename(inFile,
                TempDir, stop_compilation, output_suffix).str();

            vec.push_back(std::make_pair(65536, out_file));
        }

        if (!AutoGeneratedSinkOption.empty()) {
            for (cl::list<std::string>::iterator B = AutoGeneratedSinkOption.begin(), E = AutoGeneratedSinkOption.end(); B != E; ++B)
                vec.push_back(std::make_pair(AutoGeneratedSinkOption.getPosition(B - AutoGeneratedSinkOption.begin()), *B));
        }
        return Action(cmd, this->SortArgs(vec), stop_compilation, out_file);
    }

};
const char* clang_cpp::InputLanguages_[] = {"c++", 0};

class clang_objective_c : public Tool {
private:
    static const char* InputLanguages_[];

public:
    const char* Name() const {
        return "clang_objective_c";
    }

    const char** InputLanguages() const {
        return InputLanguages_;
    }

    const char* OutputLanguage() const {
        return "llvm-bitcode";
    }

    bool IsJoin() const {
        return false;
    }

    bool WorksOnEmpty() const {
        return false;
    }

    Action GenerateAction(const PathVector& inFiles,
        bool HasChildren,
        const llvm::sys::Path& TempDir,
        const InputLanguagesSet& InLangs,
        const LanguageMap& LangMap) const
    {
        throw std::runtime_error("clang_objective_c is not a Join tool!");
    }

    Action GenerateAction(const sys::Path& inFile,
        bool HasChildren,
        const llvm::sys::Path& TempDir,
        const InputLanguagesSet& InLangs,
        const LanguageMap& LangMap) const
    {
        std::string cmd;
        std::string out_file;
        std::vector<std::pair<unsigned, std::string> > vec;
        bool stop_compilation = !HasChildren;
        bool no_out_file = false;
        const char* output_suffix = "bc";

        cmd = "clang";
        vec.push_back(std::make_pair(0, "-x"));
        vec.push_back(std::make_pair(0, "objective-c"));

        if (AutoGeneratedSwitch_E) {
            vec.push_back(std::make_pair(AutoGeneratedSwitch_E.getPosition(), "-E"));
            stop_compilation = true;
            output_suffix = "mi";
        }
        if ((AutoGeneratedSwitch_E)
             && (OutputFilename.empty())) {
            no_out_file = true;
        }
        if (AutoGeneratedSwitch_fsyntax_dash_only) {
            stop_compilation = true;
        }
        if (AutoGeneratedSwitch_S && AutoGeneratedSwitch_emit_dash_llvm) {
            vec.push_back(std::make_pair(65536, "-emit-llvm"));
            stop_compilation = true;
            output_suffix = "ll";
        }
        if (! (AutoGeneratedSwitch_S && AutoGeneratedSwitch_emit_dash_llvm)) {
            vec.push_back(std::make_pair(65536, "-emit-llvm-bc"));
        }
        if (AutoGeneratedSwitch_c && AutoGeneratedSwitch_emit_dash_llvm) {
            stop_compilation = true;
        }
        if (!AutoGeneratedList_include.empty()) {
            for (cl::list<std::string>::iterator B = AutoGeneratedList_include.begin(),
            E = AutoGeneratedList_include.end() ; B != E;) {
                unsigned pos = AutoGeneratedList_include.getPosition(B - AutoGeneratedList_include.begin());
                vec.push_back(std::make_pair(pos, "-include"));
                vec.push_back(std::make_pair(pos, *B));
                ++B;
            }
        }
        if (!AutoGeneratedList_I.empty()) {
            for (cl::list<std::string>::iterator B = AutoGeneratedList_I.begin(),
            E = AutoGeneratedList_I.end() ; B != E;) {
                unsigned pos = AutoGeneratedList_I.getPosition(B - AutoGeneratedList_I.begin());
                vec.push_back(std::make_pair(pos, "-I"));
                vec.push_back(std::make_pair(pos, *B));
                ++B;
            }
        }

        vec.push_back(std::make_pair(InputFilenames.getPosition(0), inFile.str()));
        if (!no_out_file) {
            vec.push_back(std::make_pair(65536, "-o"));
            out_file = this->OutFilename(inFile,
                TempDir, stop_compilation, output_suffix).str();

            vec.push_back(std::make_pair(65536, out_file));
        }

        if (!AutoGeneratedSinkOption.empty()) {
            for (cl::list<std::string>::iterator B = AutoGeneratedSinkOption.begin(), E = AutoGeneratedSinkOption.end(); B != E; ++B)
                vec.push_back(std::make_pair(AutoGeneratedSinkOption.getPosition(B - AutoGeneratedSinkOption.begin()), *B));
        }
        return Action(cmd, this->SortArgs(vec), stop_compilation, out_file);
    }

};
const char* clang_objective_c::InputLanguages_[] = {"objective-c", 0};

class clang_objective_cpp : public Tool {
private:
    static const char* InputLanguages_[];

public:
    const char* Name() const {
        return "clang_objective_cpp";
    }

    const char** InputLanguages() const {
        return InputLanguages_;
    }

    const char* OutputLanguage() const {
        return "llvm-bitcode";
    }

    bool IsJoin() const {
        return false;
    }

    bool WorksOnEmpty() const {
        return false;
    }

    Action GenerateAction(const PathVector& inFiles,
        bool HasChildren,
        const llvm::sys::Path& TempDir,
        const InputLanguagesSet& InLangs,
        const LanguageMap& LangMap) const
    {
        throw std::runtime_error("clang_objective_cpp is not a Join tool!");
    }

    Action GenerateAction(const sys::Path& inFile,
        bool HasChildren,
        const llvm::sys::Path& TempDir,
        const InputLanguagesSet& InLangs,
        const LanguageMap& LangMap) const
    {
        std::string cmd;
        std::string out_file;
        std::vector<std::pair<unsigned, std::string> > vec;
        bool stop_compilation = !HasChildren;
        bool no_out_file = false;
        const char* output_suffix = "bc";

        cmd = "clang";
        vec.push_back(std::make_pair(0, "-x"));
        vec.push_back(std::make_pair(0, "objective-c++"));

        if (AutoGeneratedSwitch_E) {
            vec.push_back(std::make_pair(AutoGeneratedSwitch_E.getPosition(), "-E"));
            stop_compilation = true;
            output_suffix = "mi";
        }
        if ((AutoGeneratedSwitch_E)
             && (OutputFilename.empty())) {
            no_out_file = true;
        }
        if (AutoGeneratedSwitch_fsyntax_dash_only) {
            stop_compilation = true;
        }
        if (AutoGeneratedSwitch_S && AutoGeneratedSwitch_emit_dash_llvm) {
            vec.push_back(std::make_pair(65536, "-emit-llvm"));
            stop_compilation = true;
            output_suffix = "ll";
        }
        if (! (AutoGeneratedSwitch_S && AutoGeneratedSwitch_emit_dash_llvm)) {
            vec.push_back(std::make_pair(65536, "-emit-llvm-bc"));
        }
        if (AutoGeneratedSwitch_c && AutoGeneratedSwitch_emit_dash_llvm) {
            stop_compilation = true;
        }
        if (!AutoGeneratedList_include.empty()) {
            for (cl::list<std::string>::iterator B = AutoGeneratedList_include.begin(),
            E = AutoGeneratedList_include.end() ; B != E;) {
                unsigned pos = AutoGeneratedList_include.getPosition(B - AutoGeneratedList_include.begin());
                vec.push_back(std::make_pair(pos, "-include"));
                vec.push_back(std::make_pair(pos, *B));
                ++B;
            }
        }
        if (!AutoGeneratedList_I.empty()) {
            for (cl::list<std::string>::iterator B = AutoGeneratedList_I.begin(),
            E = AutoGeneratedList_I.end() ; B != E;) {
                unsigned pos = AutoGeneratedList_I.getPosition(B - AutoGeneratedList_I.begin());
                vec.push_back(std::make_pair(pos, "-I"));
                vec.push_back(std::make_pair(pos, *B));
                ++B;
            }
        }

        vec.push_back(std::make_pair(InputFilenames.getPosition(0), inFile.str()));
        if (!no_out_file) {
            vec.push_back(std::make_pair(65536, "-o"));
            out_file = this->OutFilename(inFile,
                TempDir, stop_compilation, output_suffix).str();

            vec.push_back(std::make_pair(65536, out_file));
        }

        if (!AutoGeneratedSinkOption.empty()) {
            for (cl::list<std::string>::iterator B = AutoGeneratedSinkOption.begin(), E = AutoGeneratedSinkOption.end(); B != E; ++B)
                vec.push_back(std::make_pair(AutoGeneratedSinkOption.getPosition(B - AutoGeneratedSinkOption.begin()), *B));
        }
        return Action(cmd, this->SortArgs(vec), stop_compilation, out_file);
    }

};
const char* clang_objective_cpp::InputLanguages_[] = {"objective-c++", 0};

class llvm_ld : public JoinTool {
private:
    static const char* InputLanguages_[];

public:
    const char* Name() const {
        return "llvm_ld";
    }

    const char** InputLanguages() const {
        return InputLanguages_;
    }

    const char* OutputLanguage() const {
        return "executable";
    }

    bool IsJoin() const {
        return true;
    }

    bool WorksOnEmpty() const {
        return false;
    }

    Action GenerateAction(const PathVector& inFiles,
        bool HasChildren,
        const llvm::sys::Path& TempDir,
        const InputLanguagesSet& InLangs,
        const LanguageMap& LangMap) const
    {
        std::string cmd;
        std::string out_file;
        std::vector<std::pair<unsigned, std::string> > vec;
        bool stop_compilation = !HasChildren;
        bool no_out_file = false;
        const char* output_suffix = "out";

        cmd = "llvm-ld";
        vec.push_back(std::make_pair(0, "-native"));
        vec.push_back(std::make_pair(0, "-disable-internalize"));

        if (AutoGeneratedSwitch_pthread) {
            vec.push_back(std::make_pair(65536, "-lpthread"));
        }
        if (!AutoGeneratedList_L.empty()) {
            for (cl::list<std::string>::iterator B = AutoGeneratedList_L.begin(),
            E = AutoGeneratedList_L.end() ; B != E;) {
                unsigned pos = AutoGeneratedList_L.getPosition(B - AutoGeneratedList_L.begin());
                vec.push_back(std::make_pair(pos, "-L"));
                vec.push_back(std::make_pair(pos, *B));
                ++B;
            }
        }
        if (!AutoGeneratedList_l.empty()) {
            for (cl::list<std::string>::iterator B = AutoGeneratedList_l.begin(),
            E = AutoGeneratedList_l.end() ; B != E;) {
                unsigned pos = AutoGeneratedList_l.getPosition(B - AutoGeneratedList_l.begin());
                vec.push_back(std::make_pair(pos, "-l"));
                vec.push_back(std::make_pair(pos, *B));
                ++B;
            }
        }
        if (!AutoGeneratedList_Wl_comma_.empty()) {
            for (cl::list<std::string>::iterator B = AutoGeneratedList_Wl_comma_.begin(), 
                 E = AutoGeneratedList_Wl_comma_.end(); B != E; ++B)
            {
                unsigned pos = AutoGeneratedList_Wl_comma_.getPosition(B - AutoGeneratedList_Wl_comma_.begin());
                vec.push_back(std::make_pair(pos, *B));
            }
        }

        for (PathVector::const_iterator B = inFiles.begin(),
            E = inFiles.end(); B != E; ++B)
        {
            vec.push_back(std::make_pair(InputFilenames.getPosition(B - inFiles.begin()), B->str()));
        }
        if (!no_out_file) {
            vec.push_back(std::make_pair(65536, "-o"));
            out_file = this->OutFilename(sys::Path(),
                TempDir, stop_compilation, output_suffix).str();

            vec.push_back(std::make_pair(65536, out_file));
        }

        return Action(cmd, this->SortArgs(vec), stop_compilation, out_file);
    }

    Action GenerateAction(const sys::Path& inFile,
        bool HasChildren,
        const llvm::sys::Path& TempDir,
        const InputLanguagesSet& InLangs,
        const LanguageMap& LangMap) const
    {
        std::string cmd;
        std::string out_file;
        std::vector<std::pair<unsigned, std::string> > vec;
        bool stop_compilation = !HasChildren;
        bool no_out_file = false;
        const char* output_suffix = "out";

        cmd = "llvm-ld";
        vec.push_back(std::make_pair(0, "-native"));
        vec.push_back(std::make_pair(0, "-disable-internalize"));

        if (AutoGeneratedSwitch_pthread) {
            vec.push_back(std::make_pair(65536, "-lpthread"));
        }
        if (!AutoGeneratedList_L.empty()) {
            for (cl::list<std::string>::iterator B = AutoGeneratedList_L.begin(),
            E = AutoGeneratedList_L.end() ; B != E;) {
                unsigned pos = AutoGeneratedList_L.getPosition(B - AutoGeneratedList_L.begin());
                vec.push_back(std::make_pair(pos, "-L"));
                vec.push_back(std::make_pair(pos, *B));
                ++B;
            }
        }
        if (!AutoGeneratedList_l.empty()) {
            for (cl::list<std::string>::iterator B = AutoGeneratedList_l.begin(),
            E = AutoGeneratedList_l.end() ; B != E;) {
                unsigned pos = AutoGeneratedList_l.getPosition(B - AutoGeneratedList_l.begin());
                vec.push_back(std::make_pair(pos, "-l"));
                vec.push_back(std::make_pair(pos, *B));
                ++B;
            }
        }
        if (!AutoGeneratedList_Wl_comma_.empty()) {
            for (cl::list<std::string>::iterator B = AutoGeneratedList_Wl_comma_.begin(), 
                 E = AutoGeneratedList_Wl_comma_.end(); B != E; ++B)
            {
                unsigned pos = AutoGeneratedList_Wl_comma_.getPosition(B - AutoGeneratedList_Wl_comma_.begin());
                vec.push_back(std::make_pair(pos, *B));
            }
        }

        vec.push_back(std::make_pair(InputFilenames.getPosition(0), inFile.str()));
        if (!no_out_file) {
            vec.push_back(std::make_pair(65536, "-o"));
            out_file = this->OutFilename(inFile,
                TempDir, stop_compilation, output_suffix).str();

            vec.push_back(std::make_pair(65536, out_file));
        }

        return Action(cmd, this->SortArgs(vec), stop_compilation, out_file);
    }

};
const char* llvm_ld::InputLanguages_[] = {"object-code", 0};

class Edge0: public Edge {
public:
    Edge0() : Edge("clang_c") {}

    unsigned Weight(const InputLanguagesSet& InLangs) const {
        unsigned ret = 0;
        if (AutoGeneratedSwitch_clang) {
            ret += 2;
        }
        return ret;
    }

};

class Edge1: public Edge {
public:
    Edge1() : Edge("clang_cpp") {}

    unsigned Weight(const InputLanguagesSet& InLangs) const {
        unsigned ret = 0;
        if (AutoGeneratedSwitch_clang) {
            ret += 2;
        }
        return ret;
    }

};

class Edge2: public Edge {
public:
    Edge2() : Edge("clang_objective_c") {}

    unsigned Weight(const InputLanguagesSet& InLangs) const {
        unsigned ret = 0;
        if (AutoGeneratedSwitch_clang) {
            ret += 2;
        }
        return ret;
    }

};

class Edge3: public Edge {
public:
    Edge3() : Edge("clang_objective_cpp") {}

    unsigned Weight(const InputLanguagesSet& InLangs) const {
        unsigned ret = 0;
        if (AutoGeneratedSwitch_clang) {
            ret += 2;
        }
        return ret;
    }

};

class Edge8: public Edge {
public:
    Edge8() : Edge("as") {}

    unsigned Weight(const InputLanguagesSet& InLangs) const {
        unsigned ret = 0;
        if (AutoGeneratedSwitch_clang) {
            ret += 2;
        }
        return ret;
    }

};

void PopulateCompilationGraphLocal(CompilationGraph& G) {
    G.insertNode(new as());
    G.insertNode(new clang_c());
    G.insertNode(new clang_cpp());
    G.insertNode(new clang_objective_c());
    G.insertNode(new clang_objective_cpp());
    G.insertNode(new llvm_ld());

    G.insertEdge("root", new Edge0());
    G.insertEdge("root", new Edge1());
    G.insertEdge("root", new Edge2());
    G.insertEdge("root", new Edge3());
    G.insertEdge("clang_c", new SimpleEdge("llc"));
    G.insertEdge("clang_cpp", new SimpleEdge("llc"));
    G.insertEdge("clang_objective_c", new SimpleEdge("llc"));
    G.insertEdge("clang_objective_cpp", new SimpleEdge("llc"));
    G.insertEdge("llc", new Edge8());
    G.insertEdge("as", new SimpleEdge("llvm_ld"));
}

struct Plugin : public llvmc::BasePlugin {

    int Priority() const { return 1; }

    void PreprocessOptions() const
    { PreprocessOptionsLocal(); }

    void PopulateLanguageMap(LanguageMap& langMap) const
    { PopulateLanguageMapLocal(langMap); }

    void PopulateCompilationGraph(CompilationGraph& graph) const
    { PopulateCompilationGraphLocal(graph); }
};

static llvmc::RegisterPlugin<Plugin> RP;

} // End anonymous namespace.

namespace llvmc {
LLVMC_FORCE_LINKAGE_DECL(LLVMC_PLUGIN_NAME) {}
}
