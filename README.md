# MSIL Backend

LLVM 2.7 was the last release to include the MSIL backend. There has been
subsequent API changes, so work will need to be done to bring this forward
to a more current LLVM.

This doesn't currently build with a standard macOS toolchain, due to STL
version incompatibility. This work was done on Ubuntu Trusty with GCC 4.4
since GCC versions > 4.5 won't build LLVM 2.7, and 4.5 was not packaged
for Trusty.

The original MSIL backend relies on several platform- and toolchain-specific
libraries to handle LLVM lowering intrinsics like `llvm.memset` or standard 
POSIX external calls to the standard C library:

 * `KERNEL32.dll`
 * `MSVSRT.dll`

The goal of this project is to build portable .NET assemblies which target
NETSTANDARD1.6 or higher. Some of the work included here includes
implementing intrinsics like `llvm.memset` directly in `cil`.

Other work includes needing to handle POSIX calls (possibly via [Mono.Posix](https://github.com/mono/mono/tree/master/mcs/class/Mono.Posix) or by "raising" them up to .NETSTANDARD API calls. Additional work needs to be done to validating
 handling things like the `x64_fp80` IR type, which mostly maps to 
MSIL's `F` type but need to test for roundtrip precision loss.

The MSIL backend itself can be found in `lib\Target\MSIL\MSILWriter.cpp`.

It is invoked via the `llc` assembler against human-readable IR generated
either by `clang` when passed the `-emit-llvm` argument, or via the `llvm-dis`
disassembler when run against LLVM bitcode. For example:

```
llc -march=msil -cppgen=module -disable-red-zone -O3 -o sqlite.dll /home/user/sqlite/sqlite3.ll
```

For debugging purposes, it's handy to see the MSIL output written to `stdout`,
in which case you'll want to pipe the LLVM IR into `stdin` instead:

```
llc -march=msil ... < /home/user/sqlite/sqlite3.ll
```

Another interesting project with similar aims provides good insight into solving some of these same problems: [Simple-MSIL-Compiler](https://github.com/LADSoft/Simple-MSIL-Compiler)

### Low Level Virtual Machine (LLVM)

This directory and its subdirectories contain source code for the Low Level
Virtual Machine, a toolkit for the construction of highly optimized compilers,
optimizers, and runtime environments.

LLVM is open source software. You may freely distribute it under the terms of
the license agreement found in LICENSE.txt.

Please see the HTML documentation provided in docs/index.html for further
assistance with LLVM.

If you're writing a package for LLVM, see docs/Packaging.html for our
suggestions.